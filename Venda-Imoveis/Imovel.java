

public class Imovel
{
    private String endereco;
    protected double precoImovel;
    private String novaOuVelha;
    
    public Imovel(String novaOuAntiga) {
        this.novaOuVelha = novaOuAntiga;
    }
    
    public Imovel(String endereco, double precoImovel) {
        this.endereco = endereco;
        this.precoImovel = precoImovel;
    }
    
    public double descontoPrecoFinalAntiga() {
        return this.precoImovel;   
    }
    
    public String getEndereco() {
        return this.endereco;   
    }
    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    public double getPrecoImovel() {
        return this.precoImovel;   
    }
    
    public void setPrecoImovel(double precoImovel) {
        this.precoImovel = precoImovel;   
    }
    
    public String getNovaOuVelha() {
        return this.novaOuVelha;   
    }
    
    public void setNovaOuVelha(String novaOuVelha) {
        this.novaOuVelha = novaOuVelha;   
    }
}
