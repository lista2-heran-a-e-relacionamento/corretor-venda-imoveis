import java.util.Scanner;


public class Principal
{
    public static void main (String [] argrs) {
        Scanner le = new Scanner(System.in);
        
        Corretor c = cadastroCorretor();
        cadastroListaImovelCorretor(c);
        
        
        
        int resposta;
        while(true) {
            
            
        System.out.println("Informe a comissão");
        c.setComissao(le.nextDouble());
            
        System.out.println("Escolha qual casa irá vender: ");
            for(int i = 0; i < c.getImoveis().size(); i++) {
                System.out.println((i+1)+")"+ " Status: " + c.getImoveis().get(i).getNovaOuVelha());
                System.out.println((i+1)+" "+ " Valor do Imóvel: " + c.getImoveis().get(i).getPrecoImovel());
            }
        
        
        
        System.out.println("Casas antigas terá um desconto de 10% do valor total do imóvel");
        
        resposta = le.nextInt();
        
        System.out.println(c.getImoveis().get(resposta - 1).getNovaOuVelha() + " com o valor de " + c.getImoveis().get(resposta - 1).descontoPrecoFinalAntiga() +
         "." + " O valor da corretagem será: R$" + c.valorCorretagem(resposta - 1));
         
         c.removeImovel(c.getImoveis().get(resposta -1));
         
         System.out.println("Gostaria de vender mais imóvies? (S)im/(N)ão");
         if(le.next().equalsIgnoreCase("N")) {
            break;    
            }
        }
    }
    
    public static Corretor cadastroCorretor() {
        Scanner le = new Scanner(System.in);
        
        Corretor c = new Corretor();
        
        System.out.println("Informe o nome do corretor: ");
        c.setNome(le.next());
        
        System.out.println("Informe a idade do corretor: ");
        c.setIdade(le.nextInt());
        
        
        return c;
    }
    
    public static void cadastroListaImovelCorretor(Corretor c) {
        
        Scanner le = new Scanner(System.in);
        Imovel imovel;
        
        while(true) {   
            
            System.out.println("Imóvel é antigo? (S)im/(N)ão");
            String resposta = le.next();
            
            if(resposta.equalsIgnoreCase("S")) {
                imovel = new ImovelAntigo("Antiga");
            } else {
                imovel = new Imovel("Novo");   
            }
            
            System.out.println("Informe o valor do imóvel");
            imovel.setPrecoImovel(le.nextDouble());
            
            System.out.println("Informe o endereço do imóvel");
            imovel.setEndereco(le.next());
            
            c.addImovel(imovel);
            
            System.out.println("Deseja incluir mais imóveis? (S)im/(N)ão");
            if(le.next().equalsIgnoreCase("N")) {
                break;   
            }
        }
    }
}
