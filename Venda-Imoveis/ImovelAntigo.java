

public class ImovelAntigo extends Imovel
{
    
    public ImovelAntigo(String novaOuAntiga) {
        super(novaOuAntiga);
    }
    
    public ImovelAntigo(String endereco, double precoImovel) {
        super(endereco, precoImovel);   
    }
    
    @Override
    public double descontoPrecoFinalAntiga() {
        return this.precoImovel - (this.precoImovel * 0.1);   
    }

}
