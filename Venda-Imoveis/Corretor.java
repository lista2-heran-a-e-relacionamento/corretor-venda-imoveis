import java.util.ArrayList;


public class Corretor
{
    private String nome;
    private int idade;
    private double comissao;
    private ArrayList<Imovel> imoveis;
    
    public Corretor() {
        imoveis = new ArrayList<>();   
    }
    
    public Corretor (String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
        imoveis = new ArrayList<>();
    }
    
    public double valorCorretagem(int i) {
        
        return getImoveis().get(i).getPrecoImovel() * getComissao();
        
    }
    
    public void addImovel(Imovel imovel) {
        this.imoveis.add(imovel);
    }
    
    public void removeImovel(Imovel imovel) {
        this.imoveis.remove(imovel);   
    }
    
    public ArrayList<Imovel> getImoveis() {
        return this.imoveis;   
    }
    
    public String getNome() {
        return this.nome;   
    }
    
    public void setNome(String nome) {
        this.nome = nome;   
    }
    
    public double getComissao() {
        return this.comissao;   
    }
    
    public void setComissao(double comissao) {
        this.comissao = comissao / 100;   
    }
    
    public int getIdade() {
        return this.idade;   
    }
    
    public void setIdade(int idade) {
        this.idade = idade;   
    }
}
